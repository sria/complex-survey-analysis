# Complex Survey Analysis

This notebook contains code to analyse the Complex survey at the following [link](https://docs.google.com/forms/d/17-89UsUt3sZv3qkGedA9uRupK_SZpdullw1LvOe1g2Y)

## Quick start

1.) Clone this repository

```shell
git clone git@gitlab.ebi.ac.uk:sria/complex-survey-analysis.git
```

2.) Install dependencies with pip

```shell
pip install -r requirements.txt
```

3.) Install spaCy model for the English language

```shell
python -m spacy download en_core_web_sm
```
